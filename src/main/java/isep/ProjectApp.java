package isep;

public class ProjectApp {
    public static void main(String[] args) {
        Project p1 = new Project.Builder(240, 8)
                .nPeople(2).nSprints(2).nDays(2).build();
        Project p2 = new Project.Builder(241, 8)
                .nSprints(2).nPeople(2).nSprints(3).category(1).nDays(2).category(13).build();
        System.out.println(p1);
        System.out.println(p2);//Display the string.
    }
}
