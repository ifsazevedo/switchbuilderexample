package isep;
//Validity checks omitted
public class Project {
    private final int code;
    private final int maxDays;

    private final int category;
    private final int nSprints;
    private final int nDays;
    private final int nPeople;


    //the Builder pattern is more verbose than the telescoping constructor pattern,
    //so it should be used only if there are enough parameters to make it worthwhile, say
    //four or more.
    public static class Builder {
        // Required parameters
        private final int code;
        private final int maxDays;

        // Optional parameters - initialized to default values
        private int category = 0;
        private int nSprints = 0;

        private int nDays = 0;
        private int nPeople = 0;
        public Builder(int code, int maxDays) {
            this.code = code;
            this.maxDays = maxDays;
        }
        public Builder category(int val)  {
            category = val; return this;
        }
        public Builder nSprints(int val)
        { nSprints = val; return this; }

        public Builder nDays(int val)
        { nDays = val; return this; }

        public Builder nPeople(int val)
        { nPeople = val; return this; }

        public Project build() {
            return new Project(this);
        }
    }
    private Project(Builder builder) {
        code = builder.code;
        maxDays = builder.maxDays;
        category = builder.category;
        nSprints = builder.nSprints;
        nDays = builder.nDays;
        nPeople = builder.nPeople;
    }
    @Override
    public String toString(){//overriding the toString() method
        return String.format("---Project data---\n  code: %06d - maxDays: %06d - category: %06d",
                code, maxDays, category);
    }

}
